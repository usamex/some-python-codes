import time

def make_pronouncing_dict(file):
    fp = open(file)
    dictionary = dict()
    for line in fp:
        line = line.strip()
        if line.startswith('##'):
            continue
        first_space = line.find(" ")
        dictionary[line[:first_space]] = line[first_space+2:]
    fp.close()
    return dictionary

def make_word_dict(file):
    word_dicts = dict()
    for line in open(file):
        word = line.strip().upper()
        word_dicts[word] = True
    return word_dicts

def homophone(word,pro_dict):
    word1 = word[1:]
    word2 = word[0] + word[2:]
    if word1 not in pro_dict\
       or word2 not in pro_dict \
       or word not in pro_dict:
        return False
    if pro_dict[word1] == pro_dict[word2] == pro_dict[word]:
        return True
    return False

if __name__ == "__main__":
    starting = time.time()
    word_dict = make_word_dict("words.txt")
    pro_dict = make_pronouncing_dict("c06d.txt")
    for word in word_dict:
        if homophone(word,pro_dict):
            print(word,pro_dict[word])
    elapsed_time = time.time() - starting
    print("Elapsed time --->",elapsed_time)
