def recursertion(A,j):
    i = j-1
    key = A[j]
    while i >= 0 and A[i] > key:
        A[i+1] = A[i]
        i-=1
    A[i+1] = key
    if j<len(A)-1:
        return recursertion(A,j+1)
    return A

if __name__ == "__main__":
    A = [3,5,6,6,9,13,2,5,8,1]
    print(recursertion(A,1))
