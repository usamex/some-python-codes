from math import floor

def binary(A,ilk,son,key):
    if ilk >= son:
        return ilk if key < A[ilk] else ilk +1
    orta = floor((ilk+son)/2)
    if key == A[orta]:
        return orta + 1
    if key > A[orta]:
        return binary(A,orta + 1,son,key)
    return binary(A,ilk,orta - 1,key)

def insertionSort(A):
    for j in range(1,len(A)):
        key = A[j]
        istenen = binary(A,0,j-1,key)
        A.insert(istenen,key)
        del A[j+1]
    return A

if __name__ == "__main__":
    B=[1,5,3,56,3,5,27,5,2,11]
    print(insertionSort(B))
